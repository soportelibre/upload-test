package ec.gob.funcionjudicial.upload;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Cliente REST del servicio de archivos, usando Apache HTTP Client.
 * 
 * @author Ricardo Arguello <ricardo.arguello@soportelibre.com>
 */
public class FileServiceClient {

    // URLs del servicio REST
    private static final String URL_LIST = "http://localhost:8080/upload-service/rest/files/list";
    private static final String URL_DOWNLOAD = "http://localhost:8080/upload-service/rest/files/download";

    /**
     * Invoca al servicio REST para obtener el listado de todos los archivos
     * almacenados.
     * 
     * @return
     * @throws IOException
     */
    public List<String> listFiles() throws IOException {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(URL_LIST);

            try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
                HttpEntity resEntity = response.getEntity();
                String archivos = EntityUtils.toString(resEntity);
                List<String> list = parseJson(archivos);
                EntityUtils.consume(resEntity);

                return list;
            }
        }
    }

    /**
     * Invoca al servicio REST para obtener el contenido de un archivo
     * 
     * @param fileName
     * @return
     * @throws IOException
     */
    public byte[] getFile(String fileName) throws IOException {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(URL_DOWNLOAD + "?name=" + fileName);

            try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
                HttpEntity resEntity = response.getEntity();

                byte[] archivo = EntityUtils.toByteArray(resEntity);
                EntityUtils.consume(resEntity);

                return archivo;
            }
        }
    }

    /**
     * Analiza el JSON con el listado de los archivos, que tiene este formato:
     * 
     * {"archivos":[{"nombre":"upload-6538164232362398096.pdf"},{"nombre":"upload-4522260704384645352.pdf"}]}
     * 
     * @param json
     * @return
     */
    private List<String> parseJson(String json) {
        JSONObject obj = new JSONObject(json);
        JSONArray array = obj.getJSONArray("archivos");

        List<String> list = new ArrayList<>();

        for (int i = 0; i < array.length(); i++) {
            list.add(array.getJSONObject(i).getString("nombre"));
        }

        return list;
    }
}