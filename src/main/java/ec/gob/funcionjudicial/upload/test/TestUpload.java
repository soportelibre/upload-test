package ec.gob.funcionjudicial.upload.test;

import java.io.File;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

/**
 * JUnit Test para probar subir un archivo usando un servicio REST.
 * 
 * @author Ricardo Arguello <ricardo.arguello@soportelibre.com>
 */
public class TestUpload {

    // Archivo local a subir
    private static final String UPLOAD_FILE = "/var/tmp/test.pdf";

    // URL del servicio REST
    private static final String URL_UPLOAD = "http://localhost:8080/upload-service/rest/files/upload";

    // Nombre del parametro multipart que contiene el archivo
    private static final String PARAMETER_NAME = "archivo";

    /**
     * Metodo para probar subir un archivo usando un servicio REST.
     */
    @Test
    public void testUpload() {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(URL_UPLOAD);
            FileBody fileBody = new FileBody(new File(UPLOAD_FILE));

            HttpEntity reqEntity = MultipartEntityBuilder.create().addPart(PARAMETER_NAME, fileBody).build();
            httpPost.setEntity(reqEntity);

            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                HttpEntity resEntity = response.getEntity();
                //System.out.println("Subido archivo, respuesta: " + EntityUtils.toString(resEntity));
                EntityUtils.consume(resEntity);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}