package ec.gob.funcionjudicial.upload.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Test;

import ec.gob.funcionjudicial.upload.FileServiceClient;

/**
 * JUnit Test para probar la bajada de archivos usando un servicio REST.
 * 
 * @author Ricardo Arguello <ricardo.arguello@soportelibre.com>
 */
public class TestDownload {

    // En esta carpeta se guardan los archivos bajados
    private static final String DOWNLOAD_DIR = "/var/tmp/download";

    /**
     * Graba todos los archivos del servidor en una carpeta local. Primero
     * obtiene un listado de TODOS los archivos almacenados y luego los baja uno
     * por uno.
     */
    @Test
    public void testDownload() {
        try {
            FileServiceClient client = new FileServiceClient();

            //System.out.println("Obteniendo el listado de todos los archivos...");
            List<String> files = client.listFiles();
            //System.out.println("Total: " + files.size() + " archivos.");

            for (String fileName : files) {
                Path file = Paths.get(DOWNLOAD_DIR, fileName);
                Files.write(file, client.getFile(fileName));
                //System.out.println("Guardado archivo: " + file.getFileName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}